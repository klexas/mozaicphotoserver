const express = require("express");
const app = express();
const initRoutes = require("./routes/web");
var cookieSession = require('cookie-session');
const configData = require('./config.json')

app.use(express.urlencoded({ extended: true }));
//Static Client Site
app.use(express.static('./client'));

const cookieParser = require("cookie-parser");
app.use(cookieParser());

app.engine('ejs', require('ejs').renderFile);
app.set('views', './client/views');
app.set('view engine', 'ejs');

var ejs = require('ejs');

initRoutes(app);

var port = configData.port;
app.listen(port, () => {
  console.log(`Running at localhost:${port}`);
});
