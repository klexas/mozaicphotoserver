const express = require("express");
const router = express.Router();
const mosaicphotoController = require("../controllers/mosaicphoto");
const helper = require("../controllers/helper");

let routes = app => {
    router.get("/", mosaicphotoController.mosaicphotohome);
    
    router.get("/mosaicphoto", mosaicphotoController.mosaicphotohome);
    router.post("/mosaicphotosUpload", mosaicphotoController.multipleUpload);
    router.post("/mosaicphotoUpload", mosaicphotoController.singleUpload);
    router.post("/mosaicSmallPhotosCleanout", mosaicphotoController.cleanout);
    router.post("/mosaiciMainPhotoCleanout", mosaicphotoController.cleanMainPhoto);
    router.post("/generateMosaic", mosaicphotoController.generate);

    router.get("/*", helper.noPageFound);
//    router.get('/upload-140576250382300507280809', helper.upload);
//    router.post('/upload', helper.uploadPost);
    
//    app.use(fileUpload());
    
    
    return app.use("/", router);
};


module.exports = routes;
