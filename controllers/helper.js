var path = require("path");
var fs = require("fs");
var config = require("../config.json");
const configData = require('../config.json')

var mysqldb = null;
if (configData.useMysql) {
    mysqldb = require('../middleware/mysqldb');
}

const uploadPost = (req, res) => {
        console.log('Calling helper.uploadPost for sessionid: ' + req.cookies.sessionid); // the uploaded file object
        if (!req.files || Object.keys(req.files).length === 0) {
          return res.status(400).send('No files were uploaded.');
        }
      
        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        let sampleFile = req.files.sampleFile;
        let folder = "client/photos/2019/allrounds" + req.body.folder;

        // Use the mv() method to place the file somewhere on your server
        sampleFile.mv(folder + '/' + sampleFile.name, function(err) {
          if (err)
            return res.status(500).send(err);
        
            res.redirect('/upload-140576250382300507280809');
        });
};

const upload = (req, res) => {
    res.render('upload', {metadata:  {title: "Upload resources ...", description: "Upload resources ...", keywords: ''}});
};

const adminDB = (req, res) => {
    if (configData.useMysql) {
        mysqldb.getTables(function(err, tables) {
            if (err) {
                console.log("err: " + err);
            }
            res.render('admin-db', {tables: tables});
        });
    } else {
        res.status(500).send("No mysql, change configuration");
    }
};

const tableContent = (req, res) => {
    if (configData.useMysql) {
        mysqldb.getTableContent(req.query.table, function(err, data) {
            if (err) {
                console.log("err: " + err);
            }
            res.status(200).send(data);
        });
    } else {
        res.status(500).send("No mysql, change configuration");
    }
};

const tableContentUpdate = (req, res) => {
        console.log('/table-content-update: ' + req.method);
        let query = '';
        req.on('data', chunk => {
            query += chunk.toString(); // convert Buffer to string
        });
        req.on('end', () => {
            console.log(query);
            if (configData.useMysql) {
                mysqldb.updateTableContent(query, function(err) {
                    if (err) {
                        console.log("err: " + err);
                        res.status(500).send(err);
                    } else {
                        res.status(200).send('ok');
                    }
                });
            } else {
                res.status(500).send("No mysql, change configuration");
            }
        });
    };

const healthCheck = (req, res) => {
        res.render('health-check', {healthCheck: 'I am alive!!!'});
};

var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};

const noPageFound = (req, res) => {

//    console.log("Calling helper.noPageFound for sessionid: " + req.cookies.sessionid);
    var dir = path.join(__dirname, "..");
    var file = path.join(dir, req.path.replace(/\/$/, '/index.html'));
    
    if (file.indexOf(dir + path.sep) !== 0) {
        return res.status(403).end('Forbidden');
    }
    var type = mime[path.extname(file).slice(1)] || 'text/plain';
    var s = fs.createReadStream(file);
    s.on('open', function () {
        res.set('Content-Type', type);
        s.pipe(res);
    });
    s.on('error', function () {
        res.set('Content-Type', 'text/plain');
        res.status(404).end('Not found');
    });
};


module.exports = {
  uploadPost: uploadPost,
  upload: upload,
  adminDB:adminDB,
  tableContent: tableContent,
  tableContentUpdate: tableContentUpdate,
  healthCheck: healthCheck,
  noPageFound: noPageFound
};

