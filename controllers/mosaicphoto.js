const path = require("path");
const fs = require("fs");
const config = require("../config.json");
const middlewareUpload = require("../middleware/mosaicphotoupload");
const middlewareGenerate = require("../middleware/mosaicphotogenerate");
const configData = require('../config.json')
const url = require('url');
var gm = null;
if (configData.useGM) {
gm = require('gm');
}

var mysqldb = null;
if (configData.useMysql) {
    mysqldb = require('../middleware/mysqldb');
}
const rimraf = require("rimraf");
const { exec } = require("child_process");

const singleUpload = async (req, res) => {
    try {
        console.log("Calling singleUpload async for: sesionid: " + req.cookies.sessionid);
        await middlewareUpload.uploadFileMiddleware(req, res);

        if (req.files.length <= 0 || req.files.length > 1) {
            return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
        }

        if (!configData.useGM) {
            return res.redirect("/mosaicphoto?show=mainPhoto");
        }
        req.files.forEach(element => {
            let resizePhotoPath = element.destination + "thumb/" + element.filename;
            rimraf(resizePhotoPath , (err) => {
                if (err) {
                    return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
                }
                gm(element.path)
                .resize(300, 300) 
                .noProfile()
                .write(resizePhotoPath, (err2) => {
                    if (err2) {
                        return res.redirect("/mosaicphoto?show=mainPhoto"); // TODO err
                    }
                    return res.redirect("/mosaicphoto?show=mainPhoto");
                });
            });
        });
    } catch (error) {
        console.log("WTF");
        console.log(error);

        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
        }
        return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
    }
};

const multipleUpload = async (req, res) => {
    try {
        console.log("Calling multipleUpload async for sessionid: " + req.cookies.sessionid);
        await middlewareUpload.uploadFilesMiddleware(req, res);
        
        if (req.files.length <= 0) {
        return res.send(`You must select at least 1 file.`);
        }

        if (!configData.useGM) {
            return res.redirect("/mosaicphoto?show=mainPhoto");
        }
        var photoPaths = [];
        var count = 0; 
        req.files.forEach(element => {
            let resizePhotoPath = element.destination + "../color/" + element.filename;
            gm(element.path)
                .resize(1, 1) 
                .noProfile()
                .write(resizePhotoPath, (err) => {
                if (err) {
                    return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
                }
                resizePhotoPath = element.destination + "../" + element.filename;
                gm(element.path)
                    .resize(null, 200) 
                    .noProfile()
                    .write(resizePhotoPath, (err1) => {
                    if (err1) {
                        return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
                    }
                    rimraf(element.path, (err2) => {
                        if (err2) {
                            return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
                        }
                        var relativePath = path.relative(process.cwd(), resizePhotoPath);
                        photoPaths.push(relativePath);
                        if (++count == req.files.length) {
                            return res.redirect("/mosaicphoto?show=smallPhotos");
                        }
                    });
                });
            });
        });
    } catch (error) {
        console.log(error);

        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
        }
        return res.redirect("/mosaicphoto?show=smallPhotos"); // TODO err
    }
};

var createFolders = function(sessionCookie) {
    let mdir = path.join(`${__dirname}/../upload/` + sessionCookie );
    
    if (!fs.existsSync(mdir)) {
        fs.mkdirSync(mdir);
    }

    let dirs =  [   mdir + "/photos", 
                    mdir + "/photos/small",
                    mdir + "/photos/small/color",
                    mdir + "/photos/small/tmp",
                    mdir + "/photos/main", 
                    mdir + "/photos/main/thumb", 
                    mdir + "/photos/mosaic",
                    mdir + "/photos/mosaic/thumb" 
                ];
   
    for (const dir of dirs) {
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
    } 
}

const cleanMainPhoto = (req, res) => {
    console.log("Calling cleanMainPhoto for : " + req.cookies);
    
    if (!req.cookies["sessionid"]) {
        return res.send('The cookies should be enabled !!', 404);
    }

    let mainPath1 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/main/main.jpg");
    let mainPath2 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/main/thumb/main.jpg");
    let mosaicpath1 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/mosaic/mosaic-photo.jpg");
    let mosaicpath2 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/mosaic/thumb/mosaic-photo.jpg");
    rimraf.sync(mainPath1);
    rimraf.sync(mainPath2);
    rimraf.sync(mosaicpath1);
    rimraf.sync(mosaicpath2);
    return res.redirect("/mosaicphoto?show=mainPhoto");
};

const cleanout = (req, res) => {
    console.log("Calling cleanout for : " + req.cookies);
    
    if (!req.cookies["sessionid"]) {
        return res.send('The cookies should be enabled !!', 404);
    }

    let dir1 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/small/");
    let dir2 = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/color/");

    rimraf.sync(dir1);
    rimraf.sync(dir2);
    return res.redirect("/mosaicphoto?show=smallPhotos");
};


const generate = async (req, res) => {
  try {
    middlewareGenerate.generateMosaic(req, res, function (err) {
        //console.log("the callback of the middlewareGenerate.generateMosaic err: " + err);
       
        if (err) {
            return res.redirect("/mosaicphoto/?show=generateMosaic"); //TODO report error here
        } 
        let photoPath = "upload/" + req.cookies["sessionid"] + "/photos/mosaic/mosaic-photo.jpg";
        let resizePhotoPath = "upload/" + req.cookies["sessionid"] + "/photos/mosaic/thumb/mosaic-photo.jpg";
        exec("convert " + photoPath + " -resize 1200x " + resizePhotoPath, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
            }
            return res.redirect("/mosaicphoto?show=generateMosaic");
        });

    });
  } catch (error) {
    console.log("err: " + error);

    return res.end(`Error when trying to generate mosaic: ${error}`);
  }
};

const renderHomePage = (metadata, show, sessionId, res) => {
    var photoPaths = [];
    var photosFolder = "";
    var mainPhoto = "";
    var mosaicPhoto = "";
    var ejsData = { metadata: metadata, sessionId: null, data: [] };
    if (sessionId) {
        ejsData.sessionId = sessionId;
        const mainPhotosFolder = process.cwd() + '/upload/' + sessionId;
        var fileNames = fs.readdirSync( mainPhotosFolder  + "/photos/small/");
        photosFolder = '/upload/' + sessionId + "/photos/small/";
        mainPhoto = '/upload/' + sessionId + "/photos/main/thumb/main.jpg";
        mosaicPhoto  = '/upload/' + sessionId + "/photos/mosaic/thumb/mosaic-photo.jpg";

        fileNames.forEach(element => {
            var stats = fs.statSync(mainPhotosFolder  + "/photos/small/" + element);
            if (stats.isFile()) {
                photoPaths.push(element);
            }
        });
        ejsData.photos = photoPaths;

        if (!fs.existsSync(mainPhotosFolder + "/photos/main/thumb/main.jpg")) {
            mainPhoto = "";
        }
        
        if (!fs.existsSync(mainPhotosFolder + "/photos/mosaic/thumb/mosaic-photo.jpg") ) {
            mosaicPhoto = "";
        }
    }

    {
        var d = {};
        d.type = "mainPhoto";
        d.show = (show == d.type) ? "show" : "";
        d.backButton = "disabled";
        d.backButtonTarget = "";
        d.nextButton = "";
        d.nextButtonTarget = "smallPhotoaUploadCollaps";

        d.controlTitle="1. Choose The Main Photo";
        d.headingId = "mainPhotoUploadHeading";
        d.collapseId = "mainPhotoUploadCollaps";
        d.cleanFormId = "clearMainForm";
        d.cleanFormAction = "/mosaiciMainPhotoCleanout";
        d.cleanFormLabel = "Delete the uploaded main photo.";
        d.cleanFormButtonStr ="Clean!";
        
        d.uploadFormId = "mainPhotoUploadForm";
        d.uploadFormAction = "/mosaicphotoUpload";
        d.uploadFormLabel = "The Main Photo to be used as source of the Mosaic";
        d.uploadFormUploadName= "photos";
        d.uploadFormButtonStr ="Upload!";
        d.uploadInputId = "input-single-file";

        d.uploadPreviewId = "mainphoto";
        d.uploadPreviewSrc= mainPhoto;

        d.backId="back1";
        d.nextId="next1";
        d.collapseButtonId="collapseButtonId1";

        ejsData.data.push(d);
    }

    {
        var d = {};
        d.type = "smallPhotos";
        d.show = (show == d.type) ? "show" : "";
        d.backButton = "";
        d.backButtonTarget = "mainPhotoUploadCollaps";
        d.nextButton = "";
        d.nextButtonTarget = "generateMosaicCollaps";
        
        d.controlTitle="2. Choose The Small Photos";
        d.headingId = "smallPhotosUploadHeading";
        d.collapseId = "smallPhotoaUploadCollaps";
        d.cleanFormId = "clearSmallForm";
        d.cleanFormAction = "/mosaicSmallPhotosCleanout";
        d.cleanFormLabel = "Delete the uploaded photos.";
        d.cleanFormButtonStr ="Clean!";
        
        d.uploadFormId = "smallPhotosUploadForm";
        d.uploadFormAction = "/mosaicphotosUpload";
        d.uploadFormLabel = "The Small Photos to be used in Mosaic generation";
        d.uploadFormUploadName= "photos";
        d.uploadFormButtonStr ="Upload!";
        d.uploadInputId = "input-multi-file";

        d.uploadPreviewId = "multiphotos";
        d.photosFolder = photosFolder;
        d.uploadPreviewSrc= photoPaths;

        d.backId="back2";
        d.nextId="next2";
        d.collapseButtonId="collapseButtonId2";

        ejsData.data.push(d);
    }

    {
        var d = {};
        d.type = "generateMosaic";
        d.show = (show == d.type) ? "show" : "";
        d.backButton = "";
        d.backButtonTarget = "smallPhotoaUploadCollaps";
        d.nextButton = "";
        d.nextButtonTarget = "purchaseMosaicCollaps";
        
        d.controlTitle="3. Generate the Mosaic";
        d.headingId = "generateMosaicHeading";
        d.collapseId = "generateMosaicCollaps";
               
        d.generateFormId = "generateMosaicForm";
        d.generateFormAction = "/generateMosaic";
        d.generateFormLabel = "Start The Mosaic Generation";
        d.generateFormButtonStr ="Generate!";
        
        d.generatePreviewId = "generatedphoto";
        d.generatePreviewSrc = mosaicPhoto;

        d.backId="back3";
        d.nextId="next3";
        d.collapseButtonId="collapseButtonId3";

        ejsData.data.push(d);
    }

    {
        var d = {};
        d.type = "purchaseMosaic";
        d.show = (show == d.type) ? "show" : "";
        d.backButton = "";
        d.backButtonTarget = "generateMosaicCollaps";
        d.nextButton = "disabled";
        d.nextButtonTarget = "";
        
        d.controlTitle="4. Purchase the Mosaic";
        d.headingId = "purchaseMosaicHeading";
        d.collapseId = "purchaseMosaicCollaps";
               
        d.generateFormId = "purchaseMosaicForm";
        d.generateFormAction = "/purchaseMosaic";
        d.generateFormLabel = "Purchase The Mosaic Photo";
        d.generateFormButtonStr ="Buy!";
        
        d.generatePreviewId = "purchasephoto";
        d.generatePreviewSrc = "";

        d.backId="back4";
        d.nextId="next4";
        d.collapseButtonId="collapseButtonId4";

        ejsData.data.push(d);
    }


    res.render('mosaicphoto', ejsData);
}

const mosaicphotohome = (req, res) => {

    const queryObject = url.parse(req.url,true).query;
    
    var sessionId = null;
    if (req.cookies.sessionid) {
        sessionId = req.cookies.sessionid;
    }
    if (sessionId) {
        createFolders(req.cookies.sessionid);
    }
    else {
        return res.render('mosaicphoto', {metadata: {title:'title', desctiption:'description', keywords:'keywords'}, show: queryObject.show});
    }
    console.log("Loading mainphotohome for sessionid: " + sessionId);
    
    if (configData.useMysql) {
        mysqldb.getMetaData("/mosaicphoto", function(err, metadata) {
            if (err) {
                console.log("err: " + err);
            }
            
            renderHomePage();
        });
    } else {
        renderHomePage({}, queryObject.show, sessionId, res );
    }
};

module.exports = {
    singleUpload: singleUpload,
    multipleUpload: multipleUpload,
    mosaicphotohome: mosaicphotohome,
    cleanMainPhoto : cleanMainPhoto,
    cleanout : cleanout,
    generate : generate
};

