const util = require("util");
const path = require("path");
const multer = require("multer");
const fs = require("fs");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    let dir = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/small/tmp/");
    //console.log("Uploading '" + `${file.originalname}` + "' into " + dir );
    callback(null, dir );
  },
  filename: (req, file, callback) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      return callback(message, null);
    }

    var filename = `${file.originalname}`;
    callback(null, filename);
  }
});

var storageSingle = multer.diskStorage({
  destination: (req, file, callback) => {
    let dir = path.join(`${__dirname}/../upload/` + req.cookies["sessionid"] + "/photos/main/");
    //console.log("Uploading '" + `${file.originalname}` + "' into " + dir );
    callback(null, dir );
  },
  filename: (req, file, callback) => {
    const match = ["image/png", "image/jpeg"];

    if (match.indexOf(file.mimetype) === -1) {
      var message = `${file.originalname} is invalid. Only accept png/jpeg.`;
      return callback(message, null);
    }
   
    let directory = path.dirname(file.originalname);
    var filename = "main" + path.extname(file.originalname);
    callback(null, filename);
  }
});

var uploadFiles = multer({ storage: storage }).array("photos", 1000);
var uploadFile  = multer({ storage: storageSingle }).array("photos", 1);
var uploadFilesMiddleware = util.promisify(uploadFiles);
var uploadFileMiddleware  = util.promisify(uploadFile );

module.exports = {
    uploadFilesMiddleware: uploadFilesMiddleware,
    uploadFileMiddleware : uploadFileMiddleware
}
