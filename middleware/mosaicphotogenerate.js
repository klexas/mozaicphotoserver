const util = require("util");
const path = require("path");
const fs = require("fs");
const { exec } = require("child_process");
const configData = require('../config.json')

var java = null;
var poolInstance = null;
if (configData.useJava) {
    java = require("java");
    java.classpath.push("middleware/core/mosaic-photos/src/mosaic/resources/metadata-extractor-2.15.0.jar");
    java.classpath.push("middleware/core/mosaic-photos/src/mosaic/resources/xmpcore-6.0.6.jar");
    java.classpath.push("middleware/core/mosaic-photos/out/mosaic_photos.jar");
    poolInstance = java.callStaticMethodSync("mosaic.java.MosaicCreatorPool", "getInstance");
}
function createManifest(sessionid, callback) {

    let manifestFilePath = "upload/" + sessionid + "/mosaicConfig.ini";

    var commands = "echo imageDPI:Integer:200 > " + manifestFilePath;
    commands += "; ";
    commands += "echo finalImageHeightInch:Integer:25 >> " + manifestFilePath;
    commands += "; ";
    commands += "echo transparencyLevel:Integer:80 >> " + manifestFilePath;
    commands += "; ";
    commands += "echo mainImageFolder:String:upload/" + sessionid + "/photos/main >> " + manifestFilePath;
    commands += "; ";
    commands += "echo smallImageFolder:String:upload/" + sessionid + "/photos/small >> " + manifestFilePath;
    commands += "; ";
    commands += "echo mosaicImageFolder:String:upload/" + sessionid + "/photos/mosaic >> " + manifestFilePath;
    commands += "; ";
    
   
    exec(commands, (error, stdout, stderr) => {
        console.log("CreateManifest call is going to finish for  sessionId: " + sessionid);
        callback(error, manifestFilePath);
    });
}

function waitForGeneration(sessionId, callback) {
   
    java.callStaticMethod("mosaic.java.MosaicCreatorPool", "getStatusStatic", sessionId, function(err, result) {
        //console.log("java getStatus for job with sessionId: " + sessionId + ", is: " + result);
        if (result != "Finished") {
            setTimeout(waitForGeneration, 2000, sessionId, callback);
        } else {
            callback(err);
        }
    });
}

const generateMosaic = (req, res, callback) => {
    console.log("Calling middleware.generateMosaic ... ");
    if (!req.cookies["sessionid"]) {
        callback('The cookies should be enabled !!');
        return;
    }

    createManifest(req.cookies["sessionid"], function(err, manifestFilePath) {
        if (err) {
            callback(err);
        } else {
            java.callStaticMethodSync("mosaic.java.MosaicCreatorPool", "addToPoolStatic", manifestFilePath, req.cookies["sessionid"]);
            waitForGeneration(req.cookies["sessionid"], function(err1) {callback(err1) });  // TODO, we should not wait, here, the data should be return when ever it is available 
        }
    });
};

module.exports = {
    generateMosaic : generateMosaic
}
