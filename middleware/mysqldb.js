/*
 mysql>  describe gameSettings;
 +------------------+-------------+------+-----+---------+-------+
 | Field            | Type        | Null | Key | Default | Extra |
 +------------------+-------------+------+-----+---------+-------+
 | teamHome         | varchar(64) | YES  |     | NULL    |       |
 | teamGuest        | varchar(64) | YES  |     | NULL    |       |
 | teamHomeCaptain  | varchar(64) | YES  |     | NULL    |       |
 | teamGuestCaptain | varchar(64) | YES  |     | NULL    |       |
 | playersNum       | int(11)     | YES  |     | 0       |       |
 | timeControl      | int(11)     | YES  |     | NULL    |       |
 | timeBonus        | int(11)     | YES  |     | NULL    |       |
 | teamHomePoints   | float       | YES  |     | NULL    |       |
 | teamGuestPoints  | float       | YES  |     | NULL    |       |
 +------------------+-------------+------+-----+---------+-------+

 mysql> describe roles;
 +---------+-------------+------+-----+---------+-------+
 | Field   | Type        | Null | Key | Default | Extra |
 +---------+-------------+------+-----+---------+-------+
 | fbid    | varchar(40) | YES  |     | NULL    |       |
 | name    | varchar(64) | YES  |     | NULL    |       |
 | roleNum | int(11)     | YES  |     | NULL    |       |
 +---------+-------------+------+-----+---------+-------+
 ALTER TABLE roles  ADD UNIQUE `unique_roles_index`(`fbid`, `roleNum`);

 mysql> describe players;
 +----------+-------------+------+-----+---------+-------+
 | Field    | Type        | Null | Key | Default | Extra |
 +----------+-------------+------+-----+---------+-------+
 | teamName | varchar(64) | YES  |     | NULL    |       |
 | name     | varchar(64) | YES  |     | NULL    |       |
 | board    | int(11)     | YES  |     | NULL    |       |
 +----------+-------------+------+-----+---------+-------+
 ALTER TABLE players  ADD UNIQUE `unique_players_index`(`board`, `teamName`);

 mysql>  describe moves;
 +---------+-------------+------+-----+---------+-------+
 | Field   | Type        | Null | Key | Default | Extra |
 +---------+-------------+------+-----+---------+-------+
 | board   | int(11)     | YES  |     | NULL    |       |
 | moveNum | int(11)     | YES  |     | NULL    |       |
 | move    | varchar(10) | YES  |     | NULL    |       |
 | color   | char(1)     | YES  |     | NULL    |       |
 +---------+-------------+------+-----+---------+-------+
 ALTER TABLE moves ADD UNIQUE `unique_moves_index`(`board`, `moveNum`, `color`);

  mysql> describe games;
 +---------------+-------------+------+-----+-------------------+-------+
 | Field         | Type        | Null | Key | Default           | Extra |
 +---------------+-------------+------+-----+-------------------+-------+
 | board         | int(11)     | YES  |     | NULL              |       |
 | result        | varchar(10) | YES  |     | NULL              |       |
 | turn          | char(1)     | YES  |     | NULL              |       |
 | whiteClockSec | int(11)     | YES  |     | NULL              |       |
 | blackClockSec | int(11)     | YES  |     | NULL              |       |
 | timeStamp     | timestamp   | NO   |     | CURRENT_TIMESTAMP |       |
 +---------------+-------------+------+-----+-------------------+-------+
 ALTER TABLE games  ADD UNIQUE `unique_games_index`(`board`);
 */

// mysql helper
var mysql      = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
//    password : 'root',
    password : 'chessleague',
    database : 'chessclubgames',
    multipleStatements: true,
    port: 3306
});

var bonus = 0;

connection.connect();

// Connect to the DB
module.exports.connect = connection;

module.exports.getMetaData = function(url, callback) {
    connection.query("SELECT * from metadata where url='" + url + "'", function(err, rows) {
        var metadata = {title: 'Sona Irish Youth Super Cup', h1:'', description:'Armenian Chess Club with the support of Sona Vitamins have organised a unique chess tournament in Ireland. Sona Irish Youth Super Cup invites the best junior players in Ireland to play in a round-robin (all-play-all) chess tournament. The tournament is divided into age groups and the eight best players in each age group have been selected to compete.', 
                        keywords:'ICU FIDE Sona Chess Super Cup'};
        if (rows.length > 0) {
            metadata= rows[0];
        }   
        callback(err, metadata);
    });
};

// Get all users
module.exports.getUsers = function(callback){
    connection.query('SELECT * from roles', function(err, rows, fields) {
        if (!err) {
            //console.log('The tournaments: ', rows);
            callback(rows);
        } else {
            console.log('Error while performing getUsers Query.');
            callback(null);
        }
    });
};

// Get role
module.exports.getRole = function(id, callback){

    connection.query("SELECT roleNum from roles where fbid='" + id + "'", function(err, rows, fields) {
        if (!err) {
            console.log('The getRole: ', JSON.stringify(rows));
            callback(rows);
        } else {
            console.log('Error while performing getRole Query.');
            callback(null);
        }
    });
};

// Get all moves
module.exports.addUser = function(userDetails){
    connection.query("insert into roles values ('" + userDetails.id + "', " + "'" + userDetails.name + "', 1)");
};

// Get all moves
module.exports.movesHistory = function(callback){


    var sql = 'SELECT * from moves order by board, movenum;';
    sql += 'SELECT *, TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, timeStamp)) as timediff from games;';
    connection.query(sql, function(err, rows, fields) {
        if (!err) {
            //console.log('The tournaments: ', rows);
            callback(rows[0], rows[1]);
        } else {
            console.log('Error while performing moves Query.');
            callback(null, null);
        }
    });
};


// update timer
module.exports.updateTimer = function(game, callback){

    var color =  game.color === 'b' ? 'w' : 'b';
    var sql = "replace INTO games VALUES (" + game.board + ", '" + game.status + "', '" + color + "', " +
        game.whiteTimer + ", " + game.blackTimer + ",  CURRENT_TIMESTAMP)";

    sql += ";";
    sql += "SELECT *, TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, timeStamp)) as timediff from games where board = " + game.board;

    console.log(sql);
    connection.query(sql, function(err, rows) {
        if (!err) {
            console.log('The updateGame: ', rows[1]);
            callback(rows[1]);
        } else {
            console.log('Error while performing updateGame Query, err:' + err);
            callback(null);
        }
    });
};

// update timer
module.exports.updateStatus = function(st, callback){
    //"board":0,"status":"BlackWon"
    var sql = "update games set result = '" + st.status + "' where board = " + st.board;
    connection.query(sql, function(err) {
        callback(err);
    });
};

// Add a move
module.exports.addMove = function(move, callback){

    //console.log("move: " + JSON.stringify(move));
    if (move.lastMove === undefined) {
        callback([]);
        return;
    }

    var sql  = "INSERT INTO moves SELECT " + move.board + ", count(*)+1, '" + move.lastMove +
        "', '" + move.color + "', '" + move.fen + "' from moves where board = " + move.board;
    sql += ";";

    var color =  move.color === 'b' ? 'w' : 'b';
    sql += "replace INTO games VALUES (" + move.board + ", '" + move.status + "', '" + color + "', " +
                            move.whiteTimer + ", " + move.blackTimer + ",  CURRENT_TIMESTAMP)";

    sql += ";";
    sql += "SELECT *, TIME_TO_SEC(TIMEDIFF(CURRENT_TIMESTAMP, timeStamp)) as timediff from games where board = " + move.board;

    console.log(sql);
    connection.query(sql, function(err, rows) {
        if (!err) {
            console.log('The addMove: ', rows[2]);
            callback(rows[2]);
        } else {
            console.log('Error while performing addMove Query, err:' + err);
            callback(null);
        }
    });
};

//resetGames
module.exports.deletefrom = function(table, callback) {
    var sql  = "delete from " + table;
    console.log(sql);
    connection.query(sql, function(err) {
        callback(err);
    });
};

//delete From table
module.exports.resetGames = function(callback) {
    var sql  = "delete from moves; delete from games;";
    console.log(sql);
    connection.query(sql, function(err) {
        callback(err);
    });
};

/*teams:
 {
 "playersNum":"2",
 "minutes":"5",
 "bonus":"3",
 "homeTeamCaptain":"Vahe Paronyan",
 "guestTeamCaptain":"Vahe Paronyan",
 "homeTeamName":"aaa",
 "guestTeamName":"zzz",
 "guestTeamPlayer1":"asas",
 "guestTeamPlayer2":"asasas",
 "homeTeamPlayers1":"dsdsd",
 "homeTeamPlayers2":"sdsdsd"
 }
 */

// create Games
module.exports.createGames = function(teams, callback) {
    var sql  = "DELETE FROM gameSettings; DELETE from players;";

    sql += "INSERT INTO gameSettings VALUES( ";
    sql += "'" + teams.homeTeamName + "', ";
    sql += "'" + teams.guestTeamName + "', ";
    sql += "'" + teams.homeTeamCaptain + "', ";
    sql += "'" + teams.guestTeamCaptain + "', ";
    sql += teams.playersNum + ", ";
    sql += teams.minutes + ", ";
    sql += teams.bonus + ", ";
    sql += 0 + ", ";
    sql += 0 + "); ";

    var delimiter = "";
    sql += "INSERT INTO players VALUES ";

    for (var i = 0; i < teams.playersNum; i++) {
        sql += delimiter;

        sql += "(";
        sql += "'" + teams.homeTeamName + "', ";
        sql += "'" + teams["homeTeamPlayers" + parseInt(i+1)] + "', ";
        sql += parseInt(i+1);
        sql += "), ";

        sql += "(";
        sql += "'" + teams.guestTeamName + "', ";
        sql += "'" + teams["guestTeamPlayers" + parseInt(i+1)] + "', ";
        sql += parseInt(i+1);
        sql += ")";

        delimiter = ",";
        ///console.log(req.query["homeTeamPlayers" + parseInt(i+1)] + " - " + req.query["guestTeamPlayer" + parseInt(i+1)]);
    }

   
    bonus = teams.bonus;
    console.log(sql);
    connection.query(sql, function(err) {
        callback(err);
    });
};

// create Teams
module.exports.getGamesSettings = function(callback) {
    var sql  = "select * from gameSettings; select * from players order by teamName, board; select * from roles;";
    console.log(sql);
    connection.query(sql, function(err, rows) {
        bonus = rows[0][0].timeBonus;
        callback(err, rows[0][0], rows[1], rows[2]);
    });
};

// get bonus
module.exports.getBonus = function() {
    return bonus;
};




// create Teams
module.exports.getSonaPlayerSelection = function(callback) {
    connection.query("SELECT *  from sonaPlayerSelection order by ageGroup, rating  DESC", function(err, rows) {
        if (!err) {
            //console.log('Selected Players: ', JSON.stringify(rows));
            callback(null, rows);
        } else {
            console.log('Error while performing getRole Query.');
            callback(err, null);
        }
    });
};

module.exports.getSonaPlayerPairings = function(callback) {
    connection.query("select  p.agegroup as ageGroup, p.round,  (select s.name from sonaPlayerSelection as s where p.playerWhite = s.num and s.ageGroup=p.ageGroup) as whitePlayer, (select s.name from sonaPlayerSelection as s where p.playerBlack = s.num and s.ageGroup=p.ageGroup) as blackPlayer, p.result, p.time  from sonaPlayerPairings as p ", function(err, rows) {
        if (!err) {
            //console.log('Selected Players: ', JSON.stringify(rows));
            callback(null, rows);
        } else {
            console.log('Error while performing getSonaPlayerPairings Query.');
            callback(err, null);
        }
    });
};

module.exports.getTables = function(callback) {
    connection.query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA='chessclubgames'", function(err, rows) {
        if (!err) {
            //console.log('Selected Players: ', JSON.stringify(rows));
            callback(null, rows);
        } else {
            console.log('Error while performing getTables Query.');
            callback(err, null);
        }
    });
};

module.exports.getTableContent = function(name, callback) {
    connection.query("SHOW COLUMNS FROM chessclubgames." + name, function(err, columns) {
        if (!err) {
            connection.query("select * from " + name, function(err, content) {
                if (!err) {
                    //console.log('Selected Players: ', JSON.stringify(rows));
                    callback(null, {columns: columns, content: content});
                } else {
                    console.log('Error while performing getTableContent Query.');
                    callback(err, null);
                }
            });
        };
    });
};

module.exports.updateTableContent = function(query, callback) {
    connection.query(query, function(err) {
        callback(err);
    });
};


