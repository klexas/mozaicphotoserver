var setPlayers = function(num) {
    var playersHome = "", playersGuest = "";

    document.getElementById("homeTeamPlayers").innerHTML = playersHome;
    document.getElementById("guestTeamPlayers").innerHTML = playersGuest;

    for (var i = 1; i < parseInt(num) + 1; i++) {
        var guestPlayerName = glPlayers.guestTeamPlayers === undefined || glPlayers.guestTeamPlayers[i-1] === undefined ?
        "Player " + i : glPlayers.guestTeamPlayers[i-1];
        var homePlayerName = glPlayers.homeTeamPlayers === undefined || glPlayers.homeTeamPlayers[i-1] === undefined ?
        "Player " + i : glPlayers.homeTeamPlayers[i-1];

        var player = `
                <div class="col-sm-12" style="padding: 0px">
                    <label class="col-sm-3" for="guestTeamPlayers${i}">Player ${i}:</label>
                    <div class="form-group col-sm-8">
                        <input type="text" class="form-control" id="guestTeamPlayers${i}" name="guestTeamPlayers${i}" placeholder="Player ${i}" value="${guestPlayerName}">
                    </div>
                </div>`;
        playersGuest += player;

        player = `
                <div class="col-sm-12" style="padding: 0px">
                    <label class="col-sm-3" for="homeTeamPlayers${i}">Player ${i}:</label>
                    <div class="form-group col-sm-8">
                        <input type="text" class="form-control" id="homeTeamPlayers${i}" name="homeTeamPlayers${i}" placeholder="Player ${i}" value="${homePlayerName}">
                    </div>
                </div>`;
        playersHome += player;

    }
    document.getElementById("homeTeamPlayers").innerHTML = playersHome;
    document.getElementById("guestTeamPlayers").innerHTML = playersGuest;
};

$('#playersNum').on('change', function(){
    var num = $(this).find("option:selected").val();
    setPlayers(num);
});
