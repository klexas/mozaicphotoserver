
var timer = [];

// Update the count down every 1 second
var createTimer = function(id, index) {

    console.log("createTimer: id: " + id + ", index: " + index);
    return setInterval(function() {
        var time = document.getElementById(id + index).innerHTML.split(":");
        if (time[0] === "TIMEOUT") {
            clearInterval(timer[index][id]);
            return;
        }
        var min = parseInt(time[0]);
        var seconds = parseInt(time[1]);

        seconds = seconds - 1;

        if (seconds < 0) {
            min = min - 1;
            seconds = 59;
        }

        if (min < 2) {
            document.getElementById(id + index).style.color = 'red';
            document.getElementById(id + index).style.background = 'lightgray';
        }
        if (min < 0) {
            document.getElementById(id + index).innerHTML = "TIMEOUT";
            clearInterval(timer[index][id]);
            updateStatus(index);
            return;
        }

        var strMin = min;
        var strSec = seconds;
        if (min < 10) {
            strMin = "0" + min;
        }
        if (seconds < 10) {
            strSec = "0" + seconds;
        }
        document.getElementById(id + index).innerHTML = strMin + ":" + strSec;

    }, 1000);
};

var createToggle = function(id, index) {
    if (timer[index] === undefined) {
        timer[index] = {};
    }

    if (games[index].game_over()) {
        if (timer[index] && timer[index]["home"]) {
            clearInterval(timer[index]["home"]);
        }
        if (timer[index] && timer[index]["guest"]) {
            clearInterval(timer[index]["guest"]);
        }
        return;
    }

    if (id == "home") {
        timer[index]["guest"] = createTimer("guest", index);
        console.log("createToggle: timer[" + index + "][guest] " + timer[index]["guest"] );
        clearInterval(timer[index]["home"]);
    } else if (id == "guest") {
        timer[index]["home"] = createTimer("home", index);
        console.log("createToggle: timer[" + index + "][home] " + timer[index]["home"] );
        clearInterval(timer[index]["guest"]);
    }
};

var updateTime = function(timer, team, index) {
    if (timer === 'TIMEOUT') {
        document.getElementById(team + index).innerHTML = timer;
    } else {
        var strMin = timer.split(":")[0];
        var strSec = timer.split(":")[1];
        if (strMin < 10) {
            strMin = "0" + strMin;
        }
        if (strSec < 10) {
            strSec = "0" + strSec;
        }
        document.getElementById(team + index).innerHTML = strMin + ":" + strSec;
    }
};

var setClocks = function(clocks) {
    for (var index in clocks) {
        var timeWhite = clocks[index].whiteClock;
        var timeBlack = clocks[index].blackClock;

        if (index%2 === 0) {
            // even boards, white is guest, black is home
            updateTime(timeWhite, "guest", index);
            updateTime(timeBlack, "home", index);
        } else {
            // odd boards, white is home, black is guest
            updateTime(timeWhite, "home", index);
            updateTime(timeBlack, "guest", index);
        }
        var isGuest = isGuestTeam(clocks[index].board, clocks[index].color);
        var team = isGuest ? 'home' : 'guest';

        if (clocks[index].status === "InProgress") {
            createToggle(team, clocks[index].board);
        }
    }
};

var getTimer = function(index, color) {
    var timer;
    var time;
    if (isGuestTeam(index, color)) {
        time = document.getElementById("guest" + index).innerHTML.split(":");
    } else {
        time = document.getElementById("home" + index).innerHTML.split(":");
    }
    if (time[0] === "TIMEOUT") {
        timer = time[0];
    } else {
        var min = parseInt(time[0]);
        var seconds = parseInt(time[1]);
        timer = 60*min + seconds;
    }
    return timer;
};

var isTimeOut = function(index) {
    // returns 'White', 'Black' or undefined

    var timeGuest = document.getElementById("guest" + index).innerHTML.split(":");
    var timeHome = document.getElementById("home" + index).innerHTML.split(":");

    if (index%2 === 0) {
        // even boards, white is guest, black is home
        if (timeGuest[0] === "TIMEOUT") {
            return 'White';
        } else if (timeHome[0] === "TIMEOUT") {
            return 'Black';
        }
    } else {
        // odd boards, white is home, black is guest
        if (timeGuest[0] === "TIMEOUT") {
            return 'Black';
        } else if (timeHome[0] === "TIMEOUT") {
            return 'White';
        }
    }

    return undefined;
};
