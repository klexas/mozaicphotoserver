
var socket = io();

/*
 lm["cookieId"] = cookieId;
 lm["board"] = index;
 lm["lastMove"] = lastMove === undefined ? undefined : lastMove.san;
 lm["fen"] = games[index].fen();
 lm["status"] = games[index]["internalStatus"];
 lm["color"] = games[index].turn() === 'b' ? 'w' : 'b';
 lm["timer"] = timer;
 */
socket.on('singleMove', function(singleMove, clocks){

    console.log(JSON.stringify(singleMove));

    setClocks(clocks);
    if (singleMove["cookieId"] === cookieId) {
        updatePGN(singleMove);
        return;
    }
    var index = singleMove['board'];

    var move = singleMove['lastMove'];
    if (move === undefined) {
        return;
    }
    var m = games[index].move(move);

    if (m) {
        games[index]["internalStatus"] = singleMove.status;
        updatePGN(singleMove);
        updateStatus(index, false);
        updateDrawResignButton(games[index].internalStatus, index);
        onSnapEnd(index);
    }
});

socket.on('updateStatus', function(st) {
    console.log(JSON.stringify(st));
    games[st.board]["internalStatus"] = st.status;

    updateDrawResignButton(st.status, st.board);
    updateStatus(st.board, false);
});

socket.on('updateClocks', function(clocks){
    console.log(JSON.stringify(clocks));
});

socket.on('reload', function() {
    console.log("reload .... ");
    location.reload();
});

socket.on('startPosition', function(moves, clocks){
    console.log("startPosition: clocks: " + JSON.stringify(clocks) + ", moves: " + moves);
    if (moves["cookieId"] === cookieId) {
        return;
    }

    var boardMoves = {};
    setClocks(clocks);

    for(var i = 0; i < moves.length; i++) {
        if (moves[i].board <  numGames) {
            var m = games[moves[i].board].move(moves[i].move);
            if (m) {
                boardMoves[moves[i].board] = moves[i].moveNum;
                onSnapEnd(moves[i].board);
            }
        }
    }

    for(var i in clocks) {
        games[clocks[i].board].internalStatus = clocks[i].status;
    console.log('Vahe: startPosition board: ' + clocks[i].board + ', status: ' + clocks[i].status); 
    }

    for (var b in boardMoves) {
        if (isGuestTeam(clocks[b].board)) {
            document.getElementById("drawHome" + clocks[b].board).style.visibility = '';
            document.getElementById("drawGuest" + clocks[b].board).style.visibility = 'hidden';
        } else {
            document.getElementById("drawHome" + clocks[b].board).style.visibility = 'hidden';
            document.getElementById("drawGuest" + clocks[b].board).style.visibility = '';
        }

        updateDrawResignButton(clocks[b].status, clocks[b].board);
        updateStatus(b, false, moves);
    }
});

socket.on('role', function(data){
    //console.log("socket.on.role was received: " + JSON.stringify(data));
    data.role = "admin";
    manageBoard = data.role;

    if (data.role === "admin") {
        disableRoleControls('guest', false, false, false); //admin
        disableRoleControls('home', false, false, false);
        document.getElementById("settings").style.display='block';
        document.getElementById("updateSettings").style.display='block';
    } else if (data.role === "hometeam") {
        disableRoleControls('home', false, true, false); // home team captain
        disableRoleControls('guest', true, true, false);
        document.getElementById("settings").style.display='block';
        document.getElementById("updateSettings").style.display='none';
    } else if (data.role === "guestteam") {
        disableRoleControls('guest', false, true, false); // guest team captain
        disableRoleControls('home', true, true, false);
        document.getElementById("settings").style.display='block';
        document.getElementById("updateSettings").style.display='none';
    } else {
        disableRoleControls('guest', true, true, true); // viewer
        disableRoleControls('home', true, true, true);
        document.getElementById("settings").style.display='none';
        document.getElementById("updateSettings").style.display='none';
    }
    //message = `Signed in as ${data.userName}, the role: ${data.role}`;
    document.getElementById("fbuser").innerHTML = data.header;
    document.getElementById("gameSettings").innerHTML = data.settings;
    document.getElementById("restart").innerHTML = data.restart;
    document.getElementById("reload").innerHTML = data.reload;

    for (var i = 0; i < games.length; i++) {
        if (manageBoard === 'viewer') {
            document.getElementById("drawHome" + i).style.visibility = 'hidden';
            document.getElementById("drawGuest" + i).style.visibility = 'hidden';
            document.getElementById("resignHome" + i).style.visibility = 'hidden';
            document.getElementById("resignGuest" + i).style.visibility = 'hidden';
        } else if (manageBoard === "guestteam") {
            document.getElementById("drawHome" + i).style.visibility = 'hidden';
            document.getElementById("resignHome" + i).style.visibility = 'hidden';
        } else if (manageBoard === "hometeam") {
            document.getElementById("drawGuest" + i).style.visibility = 'hidden';
            document.getElementById("resignGuest" + i).style.visibility = 'hidden';
        }
    }
});

socket.on('users', function(data){
    var valHome = document.getElementById('homeTeamCaptain').value;
    var valGuest = document.getElementById('guestTeamCaptain').value;
    document.getElementById("homeTeamCaptain").innerHTML = "";
    document.getElementById("guestTeamCaptain").innerHTML = "";
    var users = '<option disabled selected value> -- select an option -- </option>';
    for (var i = 0; i < data.length; i++) {
        users += '<option>' + data[i].name + '</option>';
    }
    document.getElementById("homeTeamCaptain").innerHTML = users;
    document.getElementById("guestTeamCaptain").innerHTML = users;

    document.getElementById('homeTeamCaptain').value = valHome;
    document.getElementById('guestTeamCaptain').value = valGuest;
});

