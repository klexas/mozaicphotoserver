var statusChangeCallback = function(response) {
    if (response.status === 'connected') {
        socket.emit('fblogin', response.authResponse.accessToken);
    } else {
        socket.emit('fblogout');
    }
};

function facebookLogin() {
    FB.getLoginStatus(function(response) {
        if (response.status !== 'connected') {
            FB.login(function(){
                FB.getLoginStatus(function(response) {
                    if (response.status === 'connected') {
                        socket.emit('fblogin', response.authResponse.accessToken);
                    }
                });
            }, {scope: 'public_profile,email'});
        }
    });
}

function facebookLogout() {
    FB.getLoginStatus(function(response) {
        var accessToken = response.authResponse.accessToken;
        if (response.status === 'connected') {
            FB.logout(function(){
                socket.emit('fblogout', accessToken);
            }, {scope: 'public_profile,email'});
        }
    });
}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=314261332313824';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));